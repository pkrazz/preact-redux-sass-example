import { createStore } from 'redux';

import rootReducer from './reducers/index';

/*export default createStore( (state, action) => (
	action && ACTIONS[action.type] ? ACTIONS[action.type](state, action) : state
), INITIAL, window.devToolsExtension && window.devToolsExtension());*/

export default createStore(rootReducer);
