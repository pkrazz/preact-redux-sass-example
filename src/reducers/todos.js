import { ADD_TODO, REMOVE_TODO } from './../actions';

export default function todos(state=[], action) {
	switch (action.type) {
		case ADD_TODO:
			return [
				...state,
				{
					text: action.text
				}
			];
		case REMOVE_TODO:
			return state.filter(e => e !== action.todo);
		default:
			return state;
	}
}
